<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'al13' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '-5#(#E{WoZk|%jwvU^g``E|zP%r(w?Iz]rImd-^dwl>Q[GCX9V <mxP#&=X.Zpgo' );
define( 'SECURE_AUTH_KEY',  '$6$stU*NQCA3Jy{4?]&~ke8k$K.2dOD~,oo)Tj:>WnDL#2cftZc<p6[OH3goH<Qw' );
define( 'LOGGED_IN_KEY',    '!GNR;wXo=NBO{Fx[~Y|no:`X,zHPw^FY3~=g})H$1aUb_iB9Q=<X/C6jtY0O}ze;' );
define( 'NONCE_KEY',        '] O?R:FHqc*T;>]!(!^[Cy*r2@1VKvZMprDbUu=d~vsJ&5HB==m>e``0{`0f=`(L' );
define( 'AUTH_SALT',        'ubJ8qA2u!PCQJk4Gk#^wd1L@9Qr_V0>!}z6=ccVv7LxnkTLgR&/Xs (dCoJYJg:M' );
define( 'SECURE_AUTH_SALT', '$X[X7L;V<|!i7N;j~Gs6z23}GYqm3(P~r^JX{T([rikrV?Xj[1]aDeB/1&hOte]i' );
define( 'LOGGED_IN_SALT',   'vsonYh6(C* <kXZ?34AZY;Qy)lbpEqZqg>NW|{?*,VsIm1i,F(p^#4Z6k%g%#O}G' );
define( 'NONCE_SALT',       ';NaDu@u)/26s>cgC}RN|2~IlYF7BDvi<S^ j5+ dXgM0wi(xg&9uX!?/hkHiom71' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
