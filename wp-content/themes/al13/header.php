<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>Al13</title>

<!-- Stylesheets -->
<?php wp_head(); ?>

</head>

<!-- page wrapper -->
<body class="boxed_wrapper">


    <!-- .preloader -->
    <!-- <div class="preloader"></div> -->
    <!-- /.preloader -->


    <!-- Main Header -->
    <header class="main-header header-style-one">
        <div class="outer-container">
            <div class="outer-box clearfix">
                <div class="pull-left logo-box">
                    <figure class="logo"><a href="index.html"><img src="images/logo_2.png" alt=""></a></figure>
                </div>
                <div class="pull-right nav-toggler">
                    <button class="nav-btn"> 
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
            </div>
        </div>

        <!--Sticky Header-->
        <div class="sticky-header">
            <div class="container clearfix">
                <figure class="logo-box"><a href="index.html"><img src="images/loa_al13_small2.png" alt=""></a></figure>
                <div class="menu-area">
                    <nav class="main-menu navbar-expand-lg">
                        <div class="navbar-header">
                            <!-- Toggle Button -->      
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li class="dropdown"><a href="product.html">Product</a>
                                    
                                    <ul>
                                            <li><a href="product.html">All Product</a></li>
                                       
                                  <li class="dropdown"><a href="product.html">Windows</a>
                                            <ul>
                                                <li><a href="#">Casement</a></li>
                                                <li><a href="#">Sliding </a></li>
                                                <li><a href="#">Tilt & Turn</a></li>
                                                <li><a href="#">Slide & Fold </a></li>
                                               
                                            </ul>
                                        </li>
                                        <li class="dropdown"><a href="#">Doors</a>
                                            <ul>
                                                <li><a href="#">Single</a></li>
                                                <li><a href="#">Double</a></li>
                                               
                                            </ul>
                                        </li>
                                        <li class="dropdown"><a href="#">Facades</a>
                                            <ul>
                                                <li><a href="#">Stick System</a></li>
                                                <li><a href="#">Cap System</a></li>
                                                <li><a href="#">Semi Unitize</a></li>
                                                <li><a href="#">Unitize</a></li>
                                                <li><a href="#">Spider Glazing</a></li>
                                                <li><a href="#">Cable facade</a></li>
                                            </ul>
                                        </li>
                                        <li class="dropdown"><a href="#">Sensor Doors</a>
                                            <ul>
                                                <li><a href="#">Stick System</a></li>
                                                <li><a href="#">Cap System</a></li>
                                                <li><a href="#">Semi Unitize</a></li>
                                                <li><a href="#">Unitize</a></li>
                                                <li><a href="#">Spider Glazing</a></li>
                                                <li><a href="#">Cable facade</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="project.html">Project</a></li>  
                                
                                <li class="dropdown"><a href="#">About</a>
                                    <ul>
                                        <li><a href="about.html">About Us</a></li>
                                        <li><a href="faq.html">Faq</a></li>
                                      
                                    </ul>
                                </li>  
                                <li><a href="vr.html">Studio</a></li> 
                                  
                                                             
                                <li><a href="contact-1.html">Contact</a> </li>
                                   
                               
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div><!-- sticky-header end -->
    </header>
    <!-- End Main Header -->