<?php
 
 register_nav_menus(array(
    'header' => __("Header"), 
    'footer' => __("Footer"),
   
));


add_filter('comments_array','array_reverse');
add_filter('show_admin_bar','__return_false');
add_theme_support('post-thumbnails');
add_theme_support('widgets');

function load_stylesheet()
{
    wp_enqueue_style('style',get_template_directory_uri().'/css/style.css',array(),false,'all');
    wp_enqueue_style('responsive',get_template_directory_uri().'/css/responsive.css',array(),false,'all');

}
 
add_action('wp_enqueue_scripts','load_stylesheet');

 
function load_script()
{
    wp_enqueue_script('jquery',get_template_directory_uri().'/js/jquery.js',array(),false,false);
    wp_enqueue_script('popper',get_template_directory_uri().'/js/popper.min.js',array(),false,false);
    wp_enqueue_script('bootstrap',get_template_directory_uri().'/js/bootstrap.min.js',array(),false,false);
    wp_enqueue_script('owl',get_template_directory_uri().'/js/owl.js',array(),false,false);
    wp_enqueue_script('wow',get_template_directory_uri().'/js/wow.js',array(),false,false);
    wp_enqueue_script('validation',get_template_directory_uri().'/js/validation.js',array(),false,false);
    wp_enqueue_script('fancybox',get_template_directory_uri().'/js/jquery.fancybox.js',array(),false,false);
    wp_enqueue_script('appear',get_template_directory_uri().'/js/appear.js',array(),false,false);
    wp_enqueue_script('parallax',get_template_directory_uri().'/js/parallax.min.js',array(),false,false);
    wp_enqueue_script('mCustomScrollbar',get_template_directory_uri().'/js/jquery.mCustomScrollbar.concat.min.js',array(),false,false);
    wp_enqueue_script('themepunch',get_template_directory_uri().'/revolution/js/jquery.themepunch.revolution.min.js',array(),false,false);
    // wp_enqueue_scripts('tools',get_template_directory_uri().'/revolution/js/jquery.themepunch.tools.min.js',array(),false,false);
    // wp_enqueue_scripts('actions',get_template_directory_uri().'/revolution/js/revolution.extension.actions.min.js',array(),false,false);
     //wp_enqueue_scripts('carousel',get_template_directory_uri().'/revolution/js/revolution.extension.carousel.min.js',array(),false,false);
//     wp_enqueue_scripts('kenburn',get_template_directory_uri().'/revolution/js/revolution.extension.kenburn.min.js',array(),false,false);
//     wp_enqueue_scripts('layeranimation',get_template_directory_uri().'/revolution/js/revolution.extension.layeranimation.min.js',array(),false,false);
//     wp_enqueue_scripts('migration',get_template_directory_uri().'/revolution/js/revolution.extension.migration.min.js',array(),false,false);
//     wp_enqueue_scripts('navigation',get_template_directory_uri().'/revolution/js/revolution.extension.navigation.min.js',array(),false,false);
//     wp_enqueue_scripts('extension',get_template_directory_uri().'/revolution/js/revolution.extension.parallax.min.js',array(),false,false);
//     wp_enqueue_scripts('slideanims',get_template_directory_uri().'/revolution/js/revolution.extension.slideanims.min.js',array(),false,false);
//     wp_enqueue_scripts('video',get_template_directory_uri().'/revolution/js/revolution.extension.video.min.js',array(),false,false);
//    wp_enqueue_scripts('main',get_template_directory_uri().'/js/main-slider-script.js',array(),false,false);
    // wp_enqueue_scripts('script',get_template_directory_uri().'/js/script.js',array(),false,false);

}
 
add_action('wp_enqueue_scripts','load_script');






function logo() {
	register_sidebar(
		array(
			'name'          => __( 'logo' ),
			'id'            => 'logo',

		)
  );
}
add_action( 'widgets_init', 'logo' );
