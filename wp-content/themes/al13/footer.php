    <!-- main-footer -->
    <footer class="main-footer" style="background-image: url(images/background/footer-bg.jpg);">
        <div class="container">
            <div class="widget-section">
                <div class="row">
                    
                    <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                        <div class="logo-widget footer-widget">
                            <div class="widget-content">
                                <figure class="footer-logo"><a href="index-2.html"><?php dynamic_sidebar('logo');?></a></figure>
                                <div class="text">Nulla vitae cursus quam, nec ultrices nibh. Quisque tristique lorem ac diam laoreet auctor. Proin ac massa elit.</div>
                                <ul class="social-links">
                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                    <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                       
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                        <div class="gallery-widget footer-widget">
                            <h4 class="widget-title">flicker</h4>
                            <div class="widget-content">
                                <ul class="list clearfix">
                                    <li><figure><a href="product.html"><img src="images/resource/footer-1.jpg" alt=""></a></figure></li>
                                    <li><figure><a href="product.html"><img src="images/resource/footer-2.jpg" alt=""></a></figure></li>
                                    <li><figure><a href="product.html"><img src="images/resource/footer-3.jpg" alt=""></a></figure></li>
                                    <li><figure><a href="product.html"><img src="images/resource/footer-4.jpg" alt=""></a></figure></li>
                                    <li><figure><a href="product.html"><img src="images/resource/footer-5.jpg" alt=""></a></figure></li>
                                    <li><figure><a href="product.html"><img src="images/resource/footer-6.jpg" alt=""></a></figure></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                        <div class="contact-widget footer-widget">
                            <h4 class="widget-title">Contact us</h4>
                            <div class="widget-content">
                                <ul class="list">
                                    <li>
                                        <span>Address :</span>
                                        # 339/2-1, 1st Floor, Gate No.1, Anatharamaiah Tower  Compound, Opp Satellite Bus Stand Terminal Mysore  Road Bangalore  - 560 026
                                    </li>
                                    <li>
                                        <span>Call Us :</span>
                                        <a href="tel:+91 98455 47740">+91 98455 47740</a>
                                    </li>
                                    <li>
                                        <span>Email :</span>
                                        <a href="mailto:contact@al13.in">contact@al13.in</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom centred">
                <div class="copyright">&copy; <a href="#">diaco</a> 2019. All Rights Reserved. </div>
            </div>
        </div>
    </footer>
    <!-- main-footer end -->



<!--Scroll to top-->
<button class="scroll-top scroll-to-target" data-target="html">
    <span class="fa fa-arrow-up"></span>
</button>

</body><!-- End of .page_wrapper -->
</html>

<?php wp_footer(); ?>